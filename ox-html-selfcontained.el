
(provide 'ox-html-selfcontained)

;; self-contained html export
;; https://www.reddit.com/r/orgmode/comments/7dyywu/creating_a_selfcontained_html/
(defun ox-html-selfcontained--format-image (source attributes info)
  (format "<img src=\"data:image/%s;base64,%s\"%s />"
          (or (file-name-extension source) "")
          (base64-encode-string
           (with-temp-buffer
       (insert-file-contents-literally source)
       (buffer-string)))
          (file-name-nondirectory source)))

(define-minor-mode ox-html-selfcontained-mode
  "Toggle to enable ox-html-selfcontained-mode."
  :global t
  (if ox-html-selfcontained-mode
      (advice-add
       #'org-html--format-image :override #'ox-html-selfcontained--format-image)
    (advice-remove #'org-html--format-image #'ox-html-selfcontained--format-image)))
